import java.util.ArrayList;
import java.util.Scanner;

public class Prob3v3 {
    public static String main(String[] args) {
       String s = "";
       System.out.print("Enter n number: ");
        ArrayList<String> output = new ArrayList<String>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        output.add("n = " +n+ "\n");
        for (int i=0; i<n; i++) 
        { 
            for (int j=n-i; j>1; j--) 
            { 
                output.add(" ");
            } 
            if(i == 0)
                    output.add("*");
            else{
                output.add("*");
                for (int j=0; j<2*i-1; j++ ) 
                { 
                    output.add(" ");
                }
                output.add("*");
            }
            output.add("\n");
        }
        for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.print(s);
         return s;
    }
}
