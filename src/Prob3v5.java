import java.util.ArrayList;
import java.util.Scanner;

public class Prob3v5 {
    public static String main(String[] args) {
        String s = "";
        System.out.print("Enter n number: ");
        ArrayList<String> output = new ArrayList<String>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        output.add("n = " +n+ "\n");
        int count = 1;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                for (int j = n / 2; j >= i; j--) {
                    output.add(" ");
                }
                output.add("*");

            } else if (i <= n / 2) {
                count += 2;
                if (n % 2 == 0 && i == n / 2) {
                    count -= 2;
                    for (int j = n / 2; j >= i - 1; j--) {
                        output.add(" ");
                    }
                } else {
                    for (int j = n / 2; j >= i; j--) {
                        output.add(" ");
                    }
                }
                for (int k = 0; k < count; k++) {
                    output.add("*");
                }
            } else {
                count -= 2;
                if (n % 2 == 0) {
                    for (int j = 0; j <= i - n / 2 + 1; j++) {
                        output.add(" ");
                    }
                } else {
                    for (int j = 0; j <= i - n / 2; j++) {
                        output.add(" ");
                    }
                }
                for (int k = 0; k < count; k++) {
                    output.add("*");
                }
            }

            output.add("\n");
        }
        for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.print(s);
         return s;
    }
}
