import java.util.ArrayList;
import java.util.Scanner;

public class Prob2 {
    public static String main(String[] args) {
        String s = "";
        ArrayList<String> output = new ArrayList<String>();
        System.out.print("Enter Year : ");
        Scanner in = new Scanner(System.in);
        int year = in.nextInt();
        output.add("Year: " + year + " => ");
        if(year%400 == 0 || year%4 == 0) {
            output.add("true");
        }
        else {
            output.add("false");
        }
        for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.print(s);
         return s;
    }
}
