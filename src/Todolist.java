import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Todolist {
    
    public static void main(String[] args) throws IOException, InterruptedException {

        Path workingDirectory = Paths.get("").toAbsolutePath();
        String path = workingDirectory.getParent().toString();

        while (true) {

            System.out.println("1. Add Task");

            System.out.println("2. Delete Task");

            System.out.println("3. Display all the tasks.");

            System.out.println("4. Exit.");

            Scanner in = new Scanner(System.in);

            int input = in.nextInt();

            if (input == 1) {
                String user = "";
                System.out.print("Username: ");
                Scanner in2 = new Scanner(System.in);
                user = in2.nextLine();
                File newTask;
                System.out.print("Enter task name :");
                Scanner intask = new Scanner(System.in);
                String taskName = intask.nextLine();
                try {

                    newTask = new File(path+"/"+taskName);

                    if (newTask.createNewFile()) {
                        System.out.println("Task created: " + newTask.getName());

                    } else {
                        System.out.println("Task already exists.");
                    }

                } catch (IOException e) {
                    System.out.println("An error occurred.");
                    e.printStackTrace();
                }

                FileWriter writeTask;
                writeTask = new FileWriter(taskName);
                PrintWriter printTask = new PrintWriter(writeTask);
                clearScreen();
                printTask.println("Owner by " + user);
                System.out.println("Which program do you want to run?");
                System.out.println("1. Fizz Buzz Counting.");
                System.out.println("2. Leap Year.");
                System.out.println("3. Star Pattern.");
                System.out.println("4. Else & Finally in Python.");
                System.out.println("5. Prime Number.");
                System.out.println("6. Exit.");
                Scanner in3 = new Scanner(System.in);
                int p = in3.nextInt();
                clearScreen();
                switch (p) {
                    case 1:
                        printTask.println("1. Fizz Buzz Counting.");
                        printTask.println(Prob1.main(null));
                        System.out.println();
                        printTask.close();
                        break;
                    case 2:
                        printTask.println("2. Leap Year.");
                        printTask.println(Prob2.main(null));
                        System.out.println();
                        printTask.close();
                        break;
                    case 3:
                        printTask.println("3. Star Pattern.");
                        int starPattern = -1;
                        System.out.println("1.");
                        printStar1(3);
                        System.out.println("2.");
                        printStar2(3);
                        System.out.println("3.");
                        printStar3(3);
                        System.out.println("4.");
                        printStar4(3);
                        System.out.println("5.");
                        printStar5(3);
                        System.out.print("Select the pattern : ");

                        while (starPattern < 0 || starPattern > 5) {
                            Scanner in4 = new Scanner(System.in);
                            starPattern = in4.nextInt();
                        }
                        printTask.println("Pattern: " + starPattern);
                        switch (starPattern) {
                            case 1:
                                printTask.println(Prob3v1.main(null));
                                printTask.close();
                                break;
                            case 2:
                                printTask.println(Prob3v2.main(null));
                                printTask.close();
                                break;
                            case 3:
                                printTask.println(Prob3v3.main(null));
                                printTask.close();
                                break;
                            case 4:
                                printTask.println(Prob3v4.main(null));
                                printTask.close();
                                break;
                            case 5:
                                printTask.println(Prob3v5.main(null));
                                printTask.close();
                                break;
                        }

                        System.out.println();
                        break;
                    case 4:
                        printTask.println("4. Else & Finally in Python.");
                        printTask.println(Prob4.main(null));
                        System.out.println();
                        printTask.close();
                        break;
                    case 5:
                        printTask.println("5. Prime Number.");
                        printTask.println(Prob5.main(null));
                        System.out.println();
                        printTask.close();
                        break;
                    case 6:
                        break;
                }
            } else if (input == 2) {
                clearScreen();
                File f = new File(path);
                File[] listOfFle = f.listFiles();
                ArrayList<String> fileTxt = new ArrayList<>();
                int j = 0;
                System.out.println("All Tasks:");
                for (int i = 0; i < listOfFle.length; i++) {
                    if (listOfFle[i].isFile()) {
                        String name = listOfFle[i].getName();
                        fileTxt.add(name);
                        System.out.println((j++) + " Task " + listOfFle[i].getName());
                    }
                }
                System.out.print("Choose Task you want to delete | '000' to exit : ");
                Scanner filein = new Scanner(System.in);
                int file = filein.nextInt();
                if(file == 000){
                    continue;
                }
                File positionFile = new File(path + "/" + fileTxt.get(file));
                if (positionFile.delete()) {
                    System.out.println("Task deleted successfully");
                } else {
                    System.out.println("Failed to delete the Task");
                }
            } else if (input == 3) {
                clearScreen();
                System.out.println("All Tasks:");
                File f = new File(path);
                File[] listOfFle = f.listFiles();
                ArrayList<String> fileTxt = new ArrayList<>();
                for (int i = 0; i < listOfFle.length; i++) {
                    if (listOfFle[i].isFile()) {
                        String name = listOfFle[i].getName();
                        fileTxt.add(name);
                        System.out.println("Task " + i + " " + listOfFle[i].getName());
                    }
                }

            } else if (input == 4) {
                break;
            }

        }

    }

    private static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static void printStar1(int n) {
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    private static void printStar2(int n) {
        for (int i = n - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print(' ');
            }
            for (int k = 0; k < n - i; k++) {
                System.out.print('*');
            }
            System.out.println();
        }
    }

    private static void printStar3(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = n - i; j > 1; j--) {
                System.out.print(" ");
            }
            if (i == 0) {
                System.out.print("*");
            } else {
                System.out.print("*");
                for (int j = 0; j < 2 * i - 1; j++) {
                    System.out.print(" ");
                }
                System.out.print("*");
            }
            System.out.println();
        }
    }

    private static void printStar4(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= n; j++) {
                if (i == j) {
                    System.out.print("*");
                } else if (i + j == n - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static void printStar5(int n) {
        int count = 1;
        for (int i = 0; i < n; i++) {
            if (i == 0) {
                for (int j = n / 2; j >= i; j--) {
                    System.out.print(" ");
                }
                System.out.print("*");

            } else if (i <= n / 2) {
                count += 2;
                if (n % 2 == 0 && i == n / 2) {
                    count -= 2;
                    for (int j = n / 2; j >= i - 1; j--) {
                        System.out.print(" ");
                    }
                } else {
                    for (int j = n / 2; j >= i; j--) {
                        System.out.print(" ");
                    }
                }
                for (int k = 0; k < count; k++) {
                    System.out.print("*");
                }
            } else {
                count -= 2;
                if (n % 2 == 0) {
                    for (int j = 0; j <= i - n / 2 + 1; j++) {
                        System.out.print(" ");
                    }
                } else {
                    for (int j = 0; j <= i - n / 2; j++) {
                        System.out.print(" ");
                    }
                }
                for (int k = 0; k < count; k++) {
                    System.out.print("*");
                }
            }

            System.out.println();
        }
    }
}
