import java.util.ArrayList;
import java.util.Scanner;

public class Prob3v2 {
    public static String main(String[] args) {
       String s = "";
       System.out.print("Enter n number: ");
        ArrayList<String> output = new ArrayList<String>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        output.add("n = " +n+ "\n");
        for (int i = n-1; i>=0; i--) {
            for (int j = 0; j<i; j++) {
                output.add(" ");
            }
            for (int k = 0; k<n-i; k++) {
                output.add("*");
            }
            output.add("\n");
        }
        for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.print(s);
         return s;
    }
}
