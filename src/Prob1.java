import java.util.ArrayList;
public class Prob1{

     public static String main(String []args){
         String s = "";
         ArrayList<String> output = new ArrayList<String>();
         for (int i = 1; i<=100; i++) {
            if (i%3 == 0 && i%5 == 0) {
                output.add("FizzBuzz ");
            }
            else if(i%3 == 0) {
                output.add("Fizz ");
            }
            else if (i%5 == 0) {
                output.add("Buzz ");
            }
            else {
                output.add(i + " ");
            }
            if(i%20 == 0)
                output.add("\n");
        }
         for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.println(s);
         return s;
     }
}
