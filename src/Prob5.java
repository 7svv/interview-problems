import java.util.ArrayList;
import java.util.Scanner;

public class Prob5 {
    public static String main(String[] args) {
        String s = "";
        System.out.print("Enter n number: ");
        ArrayList<String> output = new ArrayList<String>();
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        output.add("n = " +n+ "\n");
        boolean primecheck = false;
        if (n >= 2) {
            for (int i = 2; i < n; i++) {
                primecheck = true;
                for (int j = 2; j < i; j++) {
                    if (i != j && i % j == 0) {
                        primecheck = false;
                    }
                }
                if (primecheck) {
                    output.add(i + " ");
                }
            }
        }
        for(int i=0; i<output.size(); i++){
             s += output.get(i);
         }
         System.out.print(s);
         return s;
    }
}
